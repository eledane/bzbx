<?php
class acf_field_location extends acf_field
{
	function __construct()
	{
		$this->name		= 'location';
		$this->label	= __('Location');
		$this->domain	= 'acf-location';
		$this->category	= __('Choice', $this->domain); //Basic, Content, Choice, etc
		$this->defaults	= array();
		$this->version	= '1.0';
		$this->dir		= plugin_dir_url(__FILE__);
		$this->path		= plugin_dir_path(__FILE__);
		
		parent::__construct();
		$this->load_data();
	}
	
	function render_field_settings($field)
	{
		$field	= array_merge($this->defaults, $field);
		$key	= $field['name'];
	}
	
	function render_field($field)
	{
		echo '<select name="'.$field['name'].'[]" id="live_province"><option value="">请输入省份</option></select>';
		echo '<select name="'.$field['name'].'[]" id="live_city"><option value="">请输入城市</option></select>';
		echo '<select name="'.$field['name'].'[]" id="live_district"><option value="">请输入区</option></select>';
	}
	
	function load_data()
	{
		global $wpdb;
		$this->provinces	= $wpdb->get_results('SELECT * FROM `wanli_xunhu_province`');
		$this->cities		= $wpdb->get_results('SELECT * FROM `wanli_xunhu_city`');
		$this->districts	= $wpdb->get_results('SELECT * FROM `wanli_xunhu_district`');
	}
	
	function update_value($value, $post_id, $field)
	{
		return array_combine(array('province_id', 'city_id', 'district_id'), $value);
	}
	
	function load_value($value, $post_id, $field)
	{
		$field['value']		= $value;
		$this->init_value	= $value;
		return $value;
	}
	
	function valudate_value($valid, $value, $field, $input)
	{
		if(!$valid) return $valid;
		
		if(count($value) <> 3) return false;
		
		foreach($value as $val){
			if(!$val) return false;
		}
		
		return true;
	}
}

global $field_location;
$field_location = new acf_field_location();
add_action('admin_footer', 'js_load_location_backend');
function js_load_location_backend(){
    global $field_location;
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            var location_provinces	= <?php echo json_encode($field_location->provinces); ?>;
            var location_cities		= <?php echo json_encode($field_location->cities); ?>;
            var location_districts	= <?php echo json_encode($field_location->districts); ?>;

            append_province();
            $("#live_province").val("<?php echo $field_location->init_value['province_id'];?>").change(function(){
                append_city();
                append_district();
            }).trigger("change");
            $("#live_city").val("<?php echo $field_location->init_value['city_id'];?>").change(function(){
                append_district();
            }).trigger("change");
            $("#live_district").val("<?php echo $field_location->init_value['district_id'];?>");

            function append_province(){
                for(i in location_provinces){
                    $("#live_province").append(
                        $("<option />").val(location_provinces[i].id).text(location_provinces[i].name)
                    );
                }
            }

            function append_city(){
                var selected_province = $("#live_province").val();
                $("#live_city option:not(:first)").remove();

                if(selected_province > 0){
                    for(i in location_cities){
                        if(location_cities[i].province_id == selected_province){
                            $("#live_city").append(
                                $("<option />").val(location_cities[i].id).text(location_cities[i].name)
                            );
                        }
                    }
                    $("#live_city").attr({disabled:false});
                }else{
                    $("#live_city").attr({disabled:true});
                }
            }

            function append_district(){
                var selected_city = $("#live_city").val();
                $("#live_district option:not(:first)").remove();

                if(selected_city > 0){
                    for(i in location_districts){
                        if(location_districts[i].city_id == selected_city){
                            $("#live_district").append(
                                $("<option />").val(location_districts[i].id).text(location_districts[i].name)
                            );
                        }
                    }
                    $("#live_district").attr({disabled:false});
                }else{
                    $("#live_district").attr({disabled:true});
                }
            }
        });
    </script>
    <?php
}
