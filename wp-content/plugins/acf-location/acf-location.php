<?php
/*
Plugin Name: Advanced Custom Fields: Location (Province / City / District of China)
Plugin URI: http://www.flow.asia
Description: China location field(Province/city/distrct) for Advanced Custom Fields
Version: 1.0
Author: Liubing
Author URI: http://www.eduicc.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Text Domain: acf-location
Domain Path: /languages
*/
class acf_location_plugin
{
	function __construct() 
	{
		load_plugin_textdomain('acf_location_plugin', false, dirname( plugin_basename(__FILE__) ) . '/languages/' );
		
		// Version 5+
		add_action('acf/include_field_types', array($this, 'include_field_types'));
		
		// Version 4+
		add_action('acf/register_fields', array($this, 'register_fields'));
		
		// Version 3-
		add_action('init', array($this, 'init'));
	}
	
	function init()
	{
		if(function_exists('register_field')){
			register_field('acf_location', dirname(__FILE__).'/acf-location-v3.php');
		}
	}
	
	function register_fields()
	{
		include_once('acf-location-v4.php');
	}
	
	function include_field_types()
	{
		include_once('acf-location-v5.php');
	}
	
	
}

new acf_location_plugin();