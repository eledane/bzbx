<?php
/**
 * BianZouBianXue functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 */


/**
 * Add backend config framework
 */

if ( !class_exists( 'ReduxFramework' ) && file_exists( get_template_directory() . '/inc/framework/framework.php' ) ) {
     require_once( get_template_directory() . '/inc/framework/framework.php' );
   }

  if ( !isset( $redux_demo ) && file_exists( get_template_directory() . '/inc/bzbx-options-config.php' ) ) {
     require_once( get_template_directory() . '/inc/bzbx-options-config.php' );
  }


